import axios from 'axios'
export default class CarService {
    constructor() {
        axios.defaults.baseURL = 'http://localhost:3000/api/'
    }
    getAll() {
        return axios.get('cars')
    }
    add(newCar) {
        return axios.post('cars', newCar)
    }
    get(id) {
        return axios.get('cars/' + id)
    }
    edit (car) {
        return axios.put('cars/' + car.id, car)
    }
    delete (id) {
        return axios.delete('cars/' + id)
      }
}

export const carService = new CarService()